<?php

namespace Drupal\field_config_cardinality\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the cardinality 'options_select' widget.
 *
 * @FieldWidget(
 *   id = "cardinality_options_select",
 *   label = @Translation("Select list (Cardinality)"),
 *   field_types = {
 *     "entity_reference",
 *     "list_integer",
 *     "list_float",
 *     "list_string"
 *   },
 *   multiple_values = TRUE
 * )
 */
class CardinalityOptionsSelectWidget extends OptionsSelectWidget {

  /**
   * {@inheritdoc}
   */
  protected function getEmptyLabel() {
    $cardinality_label_config = $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_label_config');

    if (!$cardinality_label_config) {
      return parent::getEmptyLabel();
    }

    $cardinality = (integer) $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_config');
    $multiple = !($cardinality === 1);

    if ($multiple) {
      // Multiple select for non-required fields.
      if (!$this->required) {
        return $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'unlimited_not_required') ?? t('- None -');
      }
    }
    else {
      // Single select for non-required fields.
      if (!$this->required) {
        return $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'limited_not_required') ?? t('- None -');
      }
      // Single select for required fields.
      if ($this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'limited_required') && $this->required) {
        return $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'limited_required');
      }
    }
  }

}
