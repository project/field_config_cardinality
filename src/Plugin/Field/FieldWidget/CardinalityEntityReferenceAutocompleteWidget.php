<?php

namespace Drupal\field_config_cardinality\Plugin\Field\FieldWidget;

use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Modifies the autocomplete widget.
 */
class CardinalityEntityReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {

  /**
   * {@inheritDoc}
   */
  public function handlesMultipleValues() {
    // Pretend to support multiple values to get a single element if
    // field_config_cardinality == 1.
    if (
      $this->fieldDefinition instanceof ThirdPartySettingsInterface
      && $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_config') == 1
    ) {
      return TRUE;
    }

    return parent::handlesMultipleValues();
  }

  /**
   * {@inheritDoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Correctly massage form values for the fake single element.
    if (
      $this->fieldDefinition instanceof ThirdPartySettingsInterface
      && $this->fieldDefinition->getThirdPartySetting('field_config_cardinality', 'cardinality_config') == 1
    ) {
      return [$values['target_id']];
    }

    return parent::massageFormValues($values, $form, $form_state);
  }

}
